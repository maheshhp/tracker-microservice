const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const Redis = require('redis');

const indexRoute = require('./src/routes/index');
const { getMessageHandler } = require('./src/routes/message');
const messageQueueClient = require('./src/utils/mq.util');

const app = express();
const connectionUrl = process.env.AMPQ_CONNECTION_STRING;
const redisClient = Redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT
});
messageQueueClient.setupConnection(connectionUrl, (connectionSetupStatus, connectionChannel) => {
  if (connectionSetupStatus) {
    messageQueueClient.setupConsumer(connectionChannel, redisClient);
  } else {
    throw new Error('Unable to connect to message queue service');
  }
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRoute);
app.use('/message', getMessageHandler(redisClient));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res) => {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
