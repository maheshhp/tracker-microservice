const amqp = require('amqplib/callback_api');
const logger = require('../trace/logger');

let retryCount = 0;

const setupConnection = (connectionUrl, connectionSetupComplete) => {
  amqp.connect(connectionUrl, (error, connection) => {
    if (error) {
      logger.error(`[AMQP] connection error: ${error}`);
      if (retryCount !== 5) {
        retryCount += 1;
        return setTimeout(setupConnection(connectionUrl, connectionSetupComplete), 1000);
      }
      connectionSetupComplete(false);
    }
    connection.on('error', (err) => {
      if (err.message !== 'Connection closing') {
        logger.error(`[AMQP] conn error: ${error}`);
      }
    });
    connection.on('close', () => {
      logger.error('[AMQP] reconnecting');
      if (retryCount !== 5) {
        retryCount += 1;
        return setTimeout(setupConnection(connectionUrl, connectionSetupComplete), 1000);
      }
      connectionSetupComplete(false);
    });
    logger.info('[AMQP] connected');
    connectionSetupComplete(true, connection);
  });
};

const setupConsumer = (connection, redisClient) => {
  connection.createChannel((error, channel) => {
    if (closeConnectionOnErr(error, connection)) return;
    channel.on('error', (err) => {
      logger.error(`[AMQP] channel error: ${err}`);
    });
    channel.on('close', () => {
      logger.info('[AMQP] channel closed');
    });
    channel.assertQueue('alert', { durable: true }, (err) => {
      if (closeConnectionOnErr(err, connection)) return;
      channel.consume('alert', (message) => {
        try {
          if (message) {
            const messageFromQueue = message.content.toString();
            const parsedMessageFromQueue = JSON.parse(message.content.toString());
            redisClient.hset('accountStatusMessage', parsedMessageFromQueue.cif, messageFromQueue);
          }
        } catch (error) {
          closeConnectionOnErr(error, connection);
        }
      }, { noAck: true });
      logger.info('[AMQP] Worker is started');
    });
  });
};

const closeConnectionOnErr = (error, connection) => {
  if (!error) return false;
  logger.error(`[AMQP] error: ${error}`);
  connection.close();
  return true;
}


module.exports = {
  setupConnection,
  setupConsumer
};
