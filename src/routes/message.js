const express = require('express');
const logger = require('../trace/logger');

const router = express.Router();

const handleMessageRequest = redisClient => async (req, res) => {
  try {
    logger.info('[AMQP] Received request for getting message for a cif from queue');
    if (req.query.cif) {
      redisClient.hget('accountStatusMessage', req.query.cif, (error, queryResult) => {
        if (error) {
          logger.error('[AMQP] Cache error');
          throw new Error('Cache error');
        }
        if (queryResult !== null) {
          logger.info('[AMQP] CIF query successful');
          redisClient.hdel('accountStatusMessage', req.query.cif);
          res.status(200);
          res.json({ message: queryResult });
        } else {
          logger.info('[AMQP] CIF not found in cache');
          res.status(404);
          res.json({ error: 'CIF not found' });
        }
      });
    } else {
      logger.info('[AMQP] Invalid request received');
      res.status(400);
      res.json({ error: 'Request should contain cif query parameter' });
    }
  } catch (err) {
    logger.error(`[AMQP] ${err}`);
    res.status(500);
    res.json({
      message: 'Failed while retrieving message details'
    });
  }
}

/* GET message from cache. */
const getMessageHandler = redisClient => router.get('/', handleMessageRequest(redisClient))


module.exports = {
  handleMessageRequest,
  getMessageHandler
};
