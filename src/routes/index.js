const express = require('express');
const logger = require('../trace/logger');

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  logger.info('[AMQP] Received request for index route');
  res.send('Welcome');
});

module.exports = router;
