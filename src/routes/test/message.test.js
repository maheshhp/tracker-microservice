const chai = require('chai');
const spies = require('chai-spies');
const { handleMessageRequest } = require('../message');

chai.use(spies);

describe('Testing get message functionality', () => {
  it('/message API should return 200 for request having cif', (done) => {
    const mockRequestObject = {
      query: {
        cif: 123
      }
    };
    const mockResponseObject = {
      status: chai.spy(),
      json: chai.spy()
    };
    const mockRedisClient = {
      hget: (key, searchKey, callback) => {
        callback(null, 123)
      },
      hdel: chai.spy()
    };
    handleMessageRequest(mockRedisClient)(mockRequestObject, mockResponseObject);
    chai.expect(mockResponseObject.status).to.have.been.called.with(200);
    chai.expect(mockResponseObject.json).to.have.been.called.with({ message: 123 });
    done();
  });
  it('/message API should return 400 for request not having cif', (done) => {
    const mockRequestObject = {
      query: {
        id: 123
      }
    };
    const mockResponseObject = {
      status: chai.spy(),
      json: chai.spy()
    };
    const mockRedisClient = {
      hget: (key, searchKey, callback) => {
        callback(null, 123)
      },
      hdel: chai.spy()
    };
    handleMessageRequest(mockRedisClient)(mockRequestObject, mockResponseObject);
    chai.expect(mockResponseObject.status).to.have.been.called.with(400);
    chai.expect(mockResponseObject.json).to.have.been.called.with({ error: 'Request should contain cif query parameter' });
    done();
  });
  it('/message API should return 404 for request having cif but not in cache', (done) => {
    const mockRequestObject = {
      query: {
        cif: 123
      }
    };
    const mockResponseObject = {
      status: chai.spy(),
      json: chai.spy()
    };
    const mockRedisClient = {
      hget: (key, searchKey, callback) => {
        callback(null, null)
      },
      hdel: chai.spy()
    };
    handleMessageRequest(mockRedisClient)(mockRequestObject, mockResponseObject);
    chai.expect(mockResponseObject.status).to.have.been.called.with(404);
    chai.expect(mockResponseObject.json).to.have.been.called.with({ error: 'CIF not found' });
    done();
  });
  it('/message API should return 500 for error in cache', (done) => {
    const mockRequestObject = {
      query: {
        cif: 123
      }
    };
    const mockResponseObject = {
      status: chai.spy(),
      json: chai.spy()
    };
    const mockRedisClient = {
      hget: (key, searchKey, callback) => {
        callback('crash', null)
      },
      hdel: chai.spy()
    };
    handleMessageRequest(mockRedisClient)(mockRequestObject, mockResponseObject);
    chai.expect(mockResponseObject.status).to.have.been.called.with(500);
    chai.expect(mockResponseObject.json).to.have.been.called.with({ message: 'Failed while retrieving message details' });
    done();
  });
});
